const express = require('express');
const Parse = require('parse/node');
const orderController = require('../../library/order');
const route = express.Router();
const Order = Parse.Object.extend("Order");

function locationOf(element, array, comparer, start, end) {
  if (array.length === 0) {
      return -1;
  }

  start = start || 0;
  end = end || array.length;
  const pivot = (start + end) >> 1;  // should be faster than dividing by 2

  const c = comparer(element, array[pivot]);
  if (end - start <= 1) { return c < 0 ? pivot - 1 : pivot; }

  if (c < 0) { return locationOf(element, array, comparer, start, pivot); }
  if (c > 0) { return locationOf(element, array, comparer, pivot, end); }
  return pivot;
}

const getAllMenuWithOrder = async(vendor) => {
  const oneMonthBefore = new Date();
  oneMonthBefore.setUTCHours(0, 0, 0, 0);
  oneMonthBefore.setUTCMonth(oneMonthBefore.getUTCMonth() - 1);
  const pipeline = [
    {
      group: {
        objectId: {
          $substr: ['$_p_menu', 5, 10]
        }
      }
    }, {
      lookup: {
        from: 'Menu',
        localField: '_id',
        foreignField: '_id',
        as: 'temp'
      }
    }, {
      unwind: {
        path: '$temp'
      }
    }, {
      match: {
        'temp._p_vendor': 'Vendor$' + vendor.id
      }
    }, {
      project: {
        objectId: '$_id',
        date: '$temp.date',
        isLunch: '$temp.isLunch'
      }
    }, {
      sort: {
        date: -1,
        isLunch: 1
      }
    }
  ];
  const query = new Parse.Query(Order);
  const menuList = await query.aggregate(pipeline);

  // binary search
  const indexToSplice = locationOf({ date: oneMonthBefore }, menuList, (a, b) => {
    if (a.date > new Date(b.date.iso)) {
      return -1;
    } else if (a.date < new Date(b.date.iso)) {
      return 1;
    } else {
      return 0;
    }
  });

  // remove all elements from index to end
  menuList.splice(indexToSplice);
  menuList.reverse();

  const menuDataList = menuList.map(menu => ({ id: menu.objectId, date: menu.date.iso, isLunch: menu.isLunch }));
  return menuDataList;
};

route.get('/list/menu/:id', async(req, res) => {
  try {
    if (req.params.id === "all") {
      const menuDataList = await getAllMenuWithOrder(res.locals.vendor);
      res.status(200).json({code: 2, message: "Retrieved menu ID list.", data: menuDataList});
    } else {
      const orderDataList = await orderController.getOrderListByMenuId(req.params.id, res.locals.sessionToken);
      res.status(200).json({code: 2, message: "Retrieved preorder list.", data: orderDataList});
    }
  } catch (error) {
    res.status(400).json(error);
  }
});

route.post('/verify', async(req, res) => {
  try {
    const receivedOrder = req.body;
    const order = await orderController.verifyScannedOrderId(receivedOrder.id, receivedOrder.menuId, receivedOrder.userId, res.locals.sessionToken);

    const orderData = await orderController.toOrderData(order, false, true);
    orderData.customer = order.get("customer").id;
    const difference = {};
    const inList = Object.keys(receivedOrder.details);
    orderData.details.forEach(detail => {
      const foundDetail = receivedOrder.details[detail.mealId];
      if (foundDetail) {
        inList.splice(inList.indexOf(detail.mealId), 1);
        if (foundDetail !== detail.quantity) {
          difference[detail.mealId] = {type: "quantity", quantity: foundDetail};
        }
      } else {
        difference[detail.mealId] = {type: "added"};
      }
    });
    inList.forEach(mealId => {
      orderData.details.push({mealId: mealId, quantity: req.body.details[mealId]});
      difference[mealId] = {type: "removed"};
    });
    res.status(200).json({
      code: 1,
      message: "Coupon has been verified. " + (Object.keys(difference).length === 0 ? "This coupon is valid and correct" : "This coupon preorder has been modifed, please check the detail. "),
      data: {order: orderData, difference: difference}
    });
  } catch (e) {
    const error = {code: e.code, message: e.message};
    if (error.code === 101 && error.message === "Object not found.") {
      error.message = "Invalid coupon! This coupon is for other food vendor.";
    }
    res.status(400).json(error);
  }
});

route.post('/approve', async(req, res) => {
  try {
    const order = await orderController.verifyScannedOrderId(req.body.id, req.body.menuId, req.body.userId, res.locals.sessionToken);
    const hasReceive = order.has("receiveTime");

    if (!hasReceive) {
      order.set("receiveTime", new Date());
    } else if (!order.has("completeTime")) {
      order.set("completeTime", new Date());
    } else {
      throw new Parse.Error(300, "The preorder from coupon has been served to the customer!");
    }

    await order.save(null, {useMasterKey: true});

    res.status(200).json({code: 1, message: "Preorder has been marked as " + (hasReceive ? "served." : "preparing.")});
  } catch (e) {
    const error = {code: e.code, message: e.message};
    if (error.code === 101 && error.message === "Object not found.") {
      error.message = "Invalid coupon! This coupon is for other food vendor.";
    }
    res.status(400).json(error);
  }
});

module.exports = route;