const express = require('express');
const orderController = require('../../library/order');
const menuController = require('../../library/menu');
const route = express.Router();

// verify if user is logged in
route.use(function(req, res, next) {
  if (res.locals.userLoggedIn === 1) {
    next();
  } else {
    const message = res.locals.userLoggedIn === 0 ? "You have not logged in!" : "Your session has expired!";
    res.status(400).json({code: 301, message: message});
  }
});

route.post('/new', async (req, res) => {
  try {
    const date = new Date(req.body.date);
    const menu = await menuController.getMenuById(req.body.menuId, true);
    const order = orderController.createOrderRecord(req.user, menu, req.body.details, date);
    await order.save(null, {sessionToken: res.locals.sessionToken});
    res.status(200).json({code: 1, message: "Successfully created and pay preorder.", id: order.id});
  } catch (error) {
    res.status(400).json(error);
  }
});

route.post('/modify/:id', async (req, res) => {
  try {
    const order = await orderController.getOrderById(req.params.id, res.locals.sessionToken);
    order.set("details", req.body.details);
    await order.save(null, {sessionToken: res.locals.sessionToken});
    res.status(200).json({code: 1, message: "Successfully modified and pay preorder", id: order.id});
  } catch (error) {
    res.status(400).json(error);
  }
});

route.post('/cancel/:id', async (req, res) => {
  try {
    const order = await orderController.getOrderById(req.params.id, res.locals.sessionToken);
    order.set("cancel", true);
    await order.save(null, {sessionToken: res.locals.sessionToken});
    res.status(200).json({code: 1, message: "Successfully cancelled preorder and refunded", id: order.id});
  } catch (error) {
    res.status(400).json(error);
  }
});

const getListRequest = async (req, res) => {
  try {
    const type = req.path === '/history' ? -1 : req.path === '/getfuture' ? 0 : 1;
    const orderDataList = await orderController.getOrderList(res.locals.sessionToken, type);
    res.status(200).json({code: 2, message: "Successfully retrieved list of preorders.", data: orderDataList});
  } catch (error) {
    res.status(400).json(error);
  }
};

route.get('/history', getListRequest);
route.get('/getall', getListRequest);
route.get('/getfuture', getListRequest);

route.get('/consumption/:date/:type', async(req, res) => {
  try {
    const dateStart = menuController.toUnifiedDate(req.params.date);
    const dateEnd = menuController.toUnifiedDate(req.params.date);
    if (req.params.type === "day") {
      dateEnd.setDate(dateEnd.getDate() + 1);
    }
    if (req.params.type === "week") {
      dateEnd.setDate(dateEnd.getDate() + 7);
    }
    if (req.params.type === "month") {
      dateEnd.setDate(dateEnd.getDate() + 30);
    }
    const dateTypeHashMap = await orderController.getOrderListByDateRange(dateStart, dateEnd, res.locals.sessionToken);
    res.status(200).json({code: 2, message: "Retrieved consumption data.", data: dateTypeHashMap});
  } catch (error) {
    res.status(400).json(error);
  }
});

module.exports = route;