const exportFunc = {};
const Parse = require('parse/node');

exportFunc.getUserById = async (userId) => {
  return await (new Parse.Query(Parse.User)).get(userId);
};

module.exports = exportFunc;