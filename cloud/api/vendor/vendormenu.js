const express = require('express');
const Parse = require('parse/node');
const menuController = require('../../library/menu');
const route = express.Router();
const Menu = Parse.Object.extend("Menu");

const verifyMenuData = async (req, res, next) => {
	try {
		const menuData = await menuController.getValidatedData(req, res.locals.vendor);
		if (menuData.message.length !== 0) {
			throw new Parse.Error(300, menuData.message);
		}
		res.locals.menuData = menuData;
		next();
	} catch(error) {
		res.status(400).json(error);
	}
};

const saveMealData = async (req, res) => {
	try {
		const menuData = res.locals.menuData;
		const menu = menuController.createMenuRecord(res.locals.vendor, menuData);
		await menu.save(null, {sessionToken: res.locals.sessionToken});
		if (menuData.id) {
			await menuController.removeExtraRelation(menu.id, res.locals.sessionToken);
		}
		res.status(menuData.id ? 200 : 201).json({code: 1, message: "Successfully updated menu", id: menu.id});
	} catch(error) {
		res.status(400).json(error);
	}
};

route.post('/new', verifyMenuData, saveMealData);

route.post('/modify/:id', verifyMenuData, saveMealData);

route.get('/alldate', async(req, res) => {
	try {
		const query = new Parse.Query(Menu);
		const oneMonthBefore = new Date();
		oneMonthBefore.setUTCHours(0, 0, 0, 0);
		oneMonthBefore.setUTCMonth(oneMonthBefore.getUTCMonth() - 1);
		query.equalTo("vendor", res.locals.vendor);
		query.greaterThanOrEqualTo("date", oneMonthBefore);
		const customDateList = await query.distinct("date");
		const customDateData = customDateList.map((customDate) => {
			return customDate.iso;
		});
		res.status(200).json({code: 2, message: "Retrieved menu date list.", data: customDateData});
	} catch (error) {
		res.status(400).json(error);
	}
});

route.get('/get/:date/:type',
	async(req, res) => {
    try{
      const menus = await menuController.getMenuByVendorAndDate(res.locals.vendor.id, req.params.date, req.params.type);
      if (menus == null || menus.length === 0) {
        res.status(200).json({code: 3, message: "No menu exists for this vendor."});
        return;
      }

      let menu = menus[0];
      let isDefault = menu.get("date") <= menuController.DEFAULT_DATE_END;

      if (menus.length === 2 && isDefault) {
        menu = menus[1];
        isDefault = false;
      }

      const menuData = await menuController.toMenuData(menu, req.params.date, true);
      res.status(200).json({code: 2, message: "Retrieved menu list for vendor.", data: menuData});
    } catch(error) {
      res.status(400).json(error);
    }
  }
);

route.delete('/delete/:id',
	function(req, res, next){
		// check if there's order exist
		next();
	},
	async function(req, res){
    try {
      await Menu.createWithoutData(req.params.id).destroy({sessionToken: res.locals.sessionToken});
			res.status(200).json({code: 1, message: "Successfully reset menu", id: req.params.id});
		} catch (error) {
			res.status(400).json(error);
		}
	}
);

module.exports = route;