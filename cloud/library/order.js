const exportFunc = {};
const Parse = require('parse/node');
const menuController = require('./menu');
const Menu = Parse.Object.extend("Menu");
const Order = Parse.Object.extend("Order");

const verifyOrderDate = (date, type) => {
  if(date < ((type === "create") ? menuController.getMinimumEditableDate() : menuController.getMinimumCancelableDate())) {
    throw new Parse.Error(300, "You can't " + type + " preorder from selected date!");
  }
};
exportFunc.verifyOrderDate = verifyOrderDate;

const verifyOrder = (menu, details, date, type) => {
  if (date) {
    verifyOrderDate(date, type);
  }
  const prices = menu.get("prices");
  let error;
  const invalid = details.some(orderDetail => {
    if (orderDetail.quantity < 1) {
      error = "You must order at least 1 meal!";
    } else if(orderDetail.quantity > 5) {
      error = "You can order at most 5 meals!";
    }

    if (!prices[orderDetail.mealId]) {
      error = "Invalid meal selected! Refreshing menu...";
    }
    return error;
  });
  if (invalid) {
    throw new Parse.Error(300, error);
  }
};
exportFunc.verifyOrder = verifyOrder;

const calculateTotal = (menu, details) => {
  const prices = menu.get("prices");
  let total = 0;
  details.forEach(orderDetail => { total += prices[orderDetail.mealId].price * orderDetail.quantity; });
  return total;
};
exportFunc.calculateTotal = calculateTotal;

const getModifiedDetail = (oldDetails, newDetails, mealsHashMap) => {
  const hasDiffMealId = (b) => {
    return (aElement) => 
      (b.filter((bElement) => (bElement.mealId === aElement.mealId)).length === 0);
  };
  const hasDiffQuantity = (b) => {
    return (aElement) => 
      (b.filter((bElement) => (bElement.mealId === aElement.mealId && bElement.quantity === aElement.quantity)).length === 0);
  };
  let differenceMessage = "";
  const removed = oldDetails.filter(hasDiffMealId(newDetails));
  if (removed.length > 0) {
    differenceMessage += " Removed ";
    removed.forEach(diffDetail => {
      differenceMessage += mealsHashMap[diffDetail.mealId] + ", ";
    });
  }

  const added = newDetails.filter(hasDiffMealId(oldDetails));
  if (added.length > 0) {
    differenceMessage += " Added ";
    added.forEach(diffDetail => {
      differenceMessage += mealsHashMap[diffDetail.mealId] + ", ";
    });
  }

  const modified = oldDetails.filter(hasDiffQuantity(newDetails));
  if (modified.length > 0) {
    differenceMessage += " Modified ";
    modified.forEach(diffDetail => {
      if(removed.indexOf(diffDetail) >= 0) {
        return;
      }
      differenceMessage += mealsHashMap[diffDetail.mealId] + " from " + diffDetail.quantity + " to " +
                           (newDetails.find(meal => meal.mealId === diffDetail.mealId).quantity) + " quantity, ";
    });
  }

  return differenceMessage;
};
exportFunc.getModifiedDetail = getModifiedDetail;

const getOrderById = async(orderId, sessionToken, withMenu, withVendor) => {
  const query = new Parse.Query(Order);
  if (withMenu) {
    query.include("menu");
  }
  if (withVendor) {
    query.include("menu.vendor");
    query.include("menu.vendor.rolePtr");
  }
  return await query.get(orderId, {sessionToken: sessionToken});
};
exportFunc.getOrderById = getOrderById;

const createOrderRecord = (user, menu, details, date) => {
  const order = new Order();

  const acl = new Parse.ACL(user);
  acl.setRoleReadAccess(menu.get("vendor").get("rolePtr"), true);
  order.setACL(acl);

  order.set("customer", user);
  order.set("menu", menu);
  order.set("menuDate", date); // temporary attribute for beforesave trigger
  order.set("details", details);
  order.set("cancel", false);

  return order;
};
exportFunc.createOrderRecord = createOrderRecord;

const toOrderData = async (order, includeVendor, includeAllMenuMeal) => {
  const orderData = { id: order.id };

  orderData.cancel = order.get("cancel");
  orderData.createDate = order.createdAt;
  orderData.lastModifyDate = order.updatedAt;
  orderData.details = order.get("details");
  orderData.receiveTime = order.get("receiveTime");
  orderData.completeTime = order.get("completeTime");
  
  const menu = order.get("menu");
  orderData.menu = {
    id: menu.id,
    date: menu.get("date"),
    isLunch: menu.get("isLunch"),
  };
  if(includeVendor) {
    const vendor = menu.get("vendor");
    orderData.menu.vendor = {
      name: vendor.get("name"),
      description: vendor.get("description")
    };
  }
  
  const meals = await menuController.getMealListFromRelation(menu);

  if (includeAllMenuMeal) {
    orderData.menu.meals = meals;
  } else {
    orderData.details = orderData.details.map(orderDetail => {
      return {
        meal: meals.find(meal => orderDetail.mealId === meal.id),
        quantity: orderDetail.quantity
      };
    });
  }

  return orderData;
};
exportFunc.toOrderData = toOrderData;

const toOrderSimpleData = (order) => {
  const orderData = { id: order.id };

  orderData.cancel = order.get("cancel");
  orderData.createDate = order.createdAt;
  orderData.lastModifyDate = order.updatedAt;
  orderData.details = order.get("details");
  
  return orderData;
};
exportFunc.toOrderSimpleData = toOrderSimpleData;

// Search type -1 = past, 0 = from now to future, 1 = all
exportFunc.getOrderList = async (sessionToken, searchType) => {
  const menuQuery = new Parse.Query(Menu);
  const query = new Parse.Query(Order);
  query.include("menu");
  query.include("menu.vendor");
  query.include("menu.meals");

  if (searchType <= 0) {
    if (searchType === 0) {
      menuQuery.greaterThanOrEqualTo("date", menuController.toUnifiedDate(new Date().toDateString()));
    } else {
      menuQuery.lessThanOrEqualTo("date", menuController.toUnifiedDate(new Date().toDateString()));
    }
    query.matchesQuery("menu", menuQuery);
  } else {
    query.limit(200);
  }

  const orderList = await query.find({sessionToken: sessionToken});
  orderList.sort(function(a, b){
    const amenu = a.get("menu"), bmenu = b.get("menu");
    const comp = (amenu.get("date") - bmenu.get("date")) * (searchType === 0 ? 1 : -1);
    if (comp !== 0) {
      return comp;
    }
    return (amenu.get("isLunch") === bmenu.get("isLunch")) ? 0 : ((amenu.get("isLunch") ? 1 : -1) * (searchType === 0 ? 1 : -1));
  });
  return Promise.all(orderList.map(async(order) => {
    return await toOrderData(order, true);
  }));
};

exportFunc.getOrderListByMenuId = async(menuId, sessionToken) => {
  const query = new Parse.Query(Order);
  const menu = await menuController.getMenuById(menuId);
  query.equalTo("menu", menu);

  const menuData = await menuController.toMenuData(menu, menu.get("date"), null, true);
  const orderList = await query.find({sessionToken: sessionToken});
  const orderDataList = orderList.map(order => toOrderSimpleData(order, true) );
  return {menu: menuData, order: orderDataList};
};

exportFunc.verifyScannedOrderId = async(orderId, menuId, userId, sessionToken) => {
  const order = await getOrderById(orderId, sessionToken, true);
  if (order.get("cancel")) {
    throw new Parse.Error(300, "Invalid coupon! The coupon has been cancelled!");
  }

  const menuDate = order.get("menu").get("date");
  const currentDate = new Date();
  const isCurrentDate = currentDate.getDate() === menuDate.getUTCDate() &&
                        currentDate.getMonth() === menuDate.getUTCMonth() &&
                        currentDate.getFullYear() === menuDate.getUTCFullYear() &&
                        ((!order.get("menu").get("isLunch")  && currentDate.getHours() < 10) ||
                         (order.get("menu").get("isLunch")  && currentDate.getHours() >= 10));

  if (!isCurrentDate) {
    let outputMessage = "Invalid coupon! The coupon is valid for ";
    outputMessage += order.get("menu").get("isLunch") ? "lunch" : "breakfast";
    outputMessage += " menu on " + menuDate.toDateString();
    throw new Parse.Error(300, outputMessage);
  }
  
  if (order.get("menu").id !== menuId) {
    throw new Parse.Error(300, "Invalid coupon! The coupon does not match with the actual menu!");
  }
  if (order.get("customer").id !== userId) {
    throw new Parse.Error(300, "Invalid coupon! The coupon does not match with the actual user ID, " + order.get("customer").id);
  }
  if (order.get("completeTime")) {
    throw new Parse.Error(300, "Invalid coupon! The preorder from coupon has been served to the customer!");
  }

  return order;
};

exportFunc.getOrderListByDateRange = async (dateStart, dateEnd, sessionToken) => {
  const menuQuery = new Parse.Query(Menu);
  const query = new Parse.Query(Order);
  query.include("menu");
  query.include("menu.meals");

  menuQuery.greaterThanOrEqualTo("date", dateStart);
  menuQuery.lessThan("date", dateEnd);

  query.matchesQuery("menu", menuQuery);
  const orderList = await query.find({sessionToken: sessionToken});
  const menuMealHashMap = {}; //mealList[menuId][mealId] = calories

  await Promise.all(orderList.map(async (order) => {
    if (menuMealHashMap[order.get("menu").id]) {
      return;
    }
    menuMealHashMap[order.get("menu").id] = {}; // to occupy it 
    menuMealHashMap[order.get("menu").id] = await menuController.getMealListFromRelation(order.get("menu"), true, true, true);
  }));

  const dateTypeHashMap = {}; //dateTypeHashMap[date~0] or [date~1] // 0 = breakfast, 1 = lunch

  orderList.forEach(order => {
    const calories = order.get("details").reduce((acc, current) => {
      return acc + current.quantity * menuMealHashMap[order.get("menu").id][current.mealId];
    }, 0);
    const hashMapIdentity = order.get("menu").get("date").toISOString() + "~" + (order.get("menu").get("isLunch") ? 0 : 1);
    if (dateTypeHashMap[hashMapIdentity] == null) {
      dateTypeHashMap[hashMapIdentity] = 0;
    }
    dateTypeHashMap[hashMapIdentity] += calories;
  });
  
  return dateTypeHashMap;
};

module.exports = exportFunc;