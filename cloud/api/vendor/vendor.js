const express = require('express');
const Parse = require('parse/node');
const vendorController = require('../../library/vendor');
const route = express.Router();
const vendorMealApi = require('./vendormeal');
const vendorMenuApi = require('./vendormenu');
const vendorOrderApi = require('./vendororder');


const getVendorId = async (req, res, next) => {
  try{
    const vendorInfo = await vendorController.getVendorByUser(req.user);
    if(vendorInfo){
      res.locals.vendor = vendorInfo;
      next();
    }else{
      res.status(400).json({code: 101, message: "Vendor info not found"});
    }
  } catch(error) {
    res.status(400).json(error);
  }
};

route.get('/info', async (req, res) => {
  try{
    const vendorInfo = await vendorController.getVendorByUser(req.user, true);
    if (vendorInfo == null) {
      res.status(400).json({code: 101, message: "Vendor info not found"});
      return;
    }
    const vendorData = vendorController.toVendorData(vendorInfo, true);
    res.status(200).json({code: 1, message: "Retrieved vendor info.", data: vendorData});
  } catch(error) {
    res.status(400).json(error);
  }
});

route.post('/updateinfo',
  async (req, res, next) => {
    try {
      res.locals.vendor = await vendorController.getVendorById(req.body.id);
      res.locals.vendorData = vendorController.getValidatedData(req);
      if (res.locals.vendorData.message.length > 0) {
        throw new Parse.Error(300, res.locals.vendorData.message);
      }
      if (res.locals.vendor.get("picture").id !== res.locals.vendorData.picture.id) {
        await res.locals.vendor.get("picture").destroy({sessionToken: res.locals.sessionToken});
      }
      next();
    } catch(error) {
      res.status(400).json(error);
    }
  }, async (req, res) => {
    try {
      const vendorInfo = await vendorController.updateVendorRecord(res.locals.vendor, res.locals.vendorData, res.locals.sessionToken);
      res.status(200).json({ code: 1, message: "Successfully updated vendor info", id: vendorInfo.id });
    } catch (error) {
      res.status(400).json(error);
    }
  }
);

route.use('/meal', getVendorId, vendorMealApi);
route.use('/menu', getVendorId, vendorMenuApi);
route.use('/order', getVendorId, vendorOrderApi);

module.exports = route;