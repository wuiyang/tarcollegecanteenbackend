const express = require('express');
const Parse = require('parse/node');
const route = express.Router();
const Credit = Parse.Object.extend("Credit");
const Vendor = Parse.Object.extend("Vendor");

function setSessionCookie(req, res, user) {
  const setting = {path: '/', httpOnly: true, sameSite: "Strict", signed: true};
  const setting2 = {path: '/', sameSite: "Strict"};
  if(req.body.rememberMe){
      setting.expires = new Date(Date.now() + 2592000000);
      setting2.expires = new Date(Date.now() + 2592000000);
  }
  res.cookie("parse.session", JSON.stringify({sessionToken: user.getSessionToken()}), setting);
  res.cookie("displayName", user.get("displayName"), setting2);
  res.cookie("accessLevel", user.get("accessLevel"), setting2);
}

function isValidInput(str, regex){
	if(str == null) {
		return false;
	}
	
	return regex.test(str.trim());
}


function getValidatedData(req, isRegister){
	const data = {};
	data.message = [];
  
	if(isValidInput(req.body.username, /^[a-zA-Z0-9\~\!\@\#\$\%\^\&\*\(\)\_\+\|\{\}\:\"\<\>\?\`\-\=\\[\]\;\'\,\.\/]{4,}$/)){
		data.displayName = req.body.username.trim();
		data.username = data.displayName.toLowerCase();
	}else{
		data.message.push({
			type: "username",
			message: (req.body.username && req.body.username.length >= 4) ? "Username can only contain standard U.S. Keyboard character key." : "Username must be at least 4 characters long."
		});
	}
	
	if(isValidInput(req.body.password, /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/)){
		data.password = req.body.password; //no need to trim password
	}else{
		data.message.push({
			type: "password",
			message: (req.body.password && req.body.password.length >= 8) ? "Password must contains one uppercase character, one lowercase character, and a number." : "Password must be at least 8 characters long."
		});
	}
	
	if(isRegister){
		if(isValidInput(req.body.email, /^[a-zA-Z0-9.\-_]{1,}@[a-zA-Z0-9.\-]{2,}[.]{1}[a-zA-Z]{2,}$/)){
			data.email = req.body.email.trim().toLowerCase();
		}else{
			data.message.push({
				type: "email",
				message: "Please enter a valid email address."
			});
		}
		
		let registerUserType = -1;

		if(req.body.collegeId){
			registerUserType = 0;
		}else if(req.body.icNumber){
			registerUserType = 1;
		}else{
			data.message.push({
				type: "popup",
				message: "Please select a valid register type."
			});
		}

		data.registerUserType = registerUserType;

		if(registerUserType === 0){
			if(isValidInput(req.body.collegeId, /^(?:(P|p|)[0-9]{4}|[0-9]{7})$/)){
				data.collegeId = req.body.collegeId.trim().toUpperCase();
			}else{
				data.message.push({
					type: "collegeId",
					message: "Student ID format are 1201234 and Staff ID format are 1234 or P1234."
				});
			}
		}else if(registerUserType === 1){
			if(isValidInput(req.body.icNumber, /^([0-9]{2}((0[1-9])|(1[0-2]))((0[1-9])|([1-2][0-9])|(3[0-1]))[0-9]{6})$/)){
				data.icNumber = req.body.icNumber.trim();
			}else{
				data.message.push({
					type: "icNumber",
					message: "I/C number format are 981231123456 (without dash '-')."
				});
			}
			if(isValidInput(req.body.vendorId, /^[0-9a-zA-Z]{10}$/)){
				data.vendorId = req.body.vendorId.trim();
			}else if(req.body.vendorId.length > 0){
				data.message.push({
					type: "vendorId",
					message: "Invalid vendor ID entered, please check again."
				});
			}
		}
	}
	return data;
}

function verifyUserData(req, res, next) {
	if(res.locals.userLoggedIn === 1){
		res.status(400).json({code: 301, message: "You have logged in!"});
	}else{
		const userData = getValidatedData(req, req.path === "/register");
		if(userData.message.length !== 0){
			res.status(400).json({code: 300, message: userData.message});
		}else{
			res.locals.userData = userData;
			next();
		}
	}
}

function getVendorIfExists(req, res, next) {
	if(res.locals.userData.registerUserType === 1 && res.locals.userData.vendorId){
		const query = new Parse.Query(Vendor);
		query.include("rolePtr");
		query.get(res.locals.userData.vendorId, {sessionToken: res.locals.sessionToken}).then(function(vendor){
			if(vendor == null){
				res.status(400).json({code: 399, message: "Vendor ID not found, are you sure you entered correct vendor ID?"});
			}else{
				res.locals.vendorObject = vendor;
				next();
			}
		}).catch(function(error){
			res.status(400).json(error);
		});
	}else{
		next();
	}
}

function addVendor(res, user){
	let vendor = res.locals.vendorObject;
	const newVendor = vendor == null;
	let role;
	if(newVendor){
		vendor = new Vendor();
		const roleAcl = new Parse.ACL();
		roleAcl.setPublicReadAccess(true);
		roleAcl.setPublicWriteAccess(false);
		role = new Parse.Role("Food Store owned by " + user.id, roleAcl);
		role.set("owner", user);
		const vendorAcl = new Parse.ACL();
		vendorAcl.setPublicReadAccess(true);
		vendorAcl.setRoleWriteAccess(role, true);
		vendor.setACL(vendorAcl);
		vendor.set("rolePtr", role);
	}else{
		role = vendor.get("rolePtr");
	}
	role.getUsers().add(user);
	//using master key as adding new user to write-protected role
	role.save(null, { useMasterKey: true }).then(function(){
		if(newVendor){
			vendor.save(null, {sessionToken: res.locals.sessionToken}).then(function(){
				res.status(201).json({code: 1, message: "Sucesss"});
			}).catch(function(error){
				res.status(400).json(error);
			});
		}else{
			res.status(201).json({code: 1, message: "Sucesss"});
		}
	}).catch(function(error){
		res.status(400).json(error);
	});
}

route.post('/login', verifyUserData,
	function(req, res){
		const userData = res.locals.userData;
		Parse.User.logIn(userData.username, userData.password).then(function(user) {
			setSessionCookie(req, res, user);
			res.status(200).json({code: 1, message: "Sucesss"});
		}).catch(function(error){
			res.status(400).json(error);
		});
	}
);

route.post('/register', verifyUserData, getVendorIfExists,
	function(req, res) {
		const userData = res.locals.userData;
		const user = new Parse.User();
		user.set("username", userData.username);
		user.set("displayName", userData.displayName);
		user.set("password", userData.password);
		user.set("email", userData.email);
		user.set("idNumber", userData.registerUserType === 0 ? userData.collegeId : userData.icNumber);
		user.set("accessLevel", userData.registerUserType);
		user.signUp().then(function(user){
			setSessionCookie(req, res, user);

			const credit = new Credit();
			const acl = new Parse.ACL(user);
			credit.setACL(acl);
			credit.set("user", user);
			credit.set("credit", 0);
			credit.save(null, { sessionToken: user.getSessionToken() });

			if(userData.registerUserType === 1){
				addVendor(res, user);
			}else{
				res.status(201).json({code: 1, message: "Sucesss"});
			}
		}).catch(function(error){
			let code = error.code;
			let message = error.message;
			if(error.code === 137){
				code = 210;
				message = "Account already exists for this staff/student ID.";
			}
			res.status(400).json({code: code, message: message});
		});
	}
);

route.get('/logout',
	function(req, res){
		if(res.locals.userLoggedIn === 1){
			Parse._request('POST', 'logout', {}, {sessionToken: res.locals.sessionToken}).then(function(){
				res.clearCookie('parse.session');
				res.clearCookie('displayName');
				res.clearCookie('accessLevel');
				res.status(200).json({code: 1, message: "Logged out."});
			}).catch(function(){
				res.status(400).json({code: 301, message: "Server error! Unable to log out."});
			});
		}else if(res.locals.userLoggedIn === 0){
			res.status(400).json({code: 301, message: "You have not login."});
		}else if(res.locals.userLoggedIn === -1){
			res.status(400).json({code: 209, message: "You session has expired."});
		}
	}
);

route.get('/info', function(req, res){
	const userData = { id: req.user.id };
	userData.displayName = req.user.get("displayName");
	userData.email = req.user.get("email");
	userData.idNumber = req.user.get("idNumber");
	res.status(200).json({code: 1, message: "Retrieved user data.", data: userData});
});

route.post('/updateinfo', function(req, res){
	const userData = req.body;
});

route.post('/changepass', async(req, res) => {
	try {
		if (res.locals.userLoggedIn === -1) {
			throw new Parse.Error(209, "You session has expired.");
		} else if (res.locals.userLoggedIn === 0) {
			throw new Parse.Error(301, "You have not login.");
		}
		const username = req.user.getUsername();
		const oldpass = req.body.oldPassword;
		const newpass = req.body.newPassword;

		if (oldpass === newpass) {
			throw new Parse.Error(300, "New password is same as old password!");
		}

		if (!isValidInput(newpass, /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/)) {
			throw new Parse.Error(300, (newpass && newpass.length >= 8) ? "Password must contains one uppercase character, one lowercase character, and a number." : "Password must be at least 8 characters long.");
		}

		const curuser = await Parse.User.logIn(username, oldpass);
		curuser.setPassword(newpass);
		await curuser.save(null, {sessionToken: curuser.getSessionToken()});

    res.clearCookie('parse.session');
    res.clearCookie('displayName');
    res.clearCookie('accessLevel');
		res.status(200).json({code: 1, message: "Successfully updated password, please login again"});
	} catch (error) {
		res.status(400).json(error);
	}
});

route.post('/reset', async(req, res) => {
	try {
		await Parse.User.requestPasswordReset(req.body.email);
		res.status(200).json({code: 1, message: "We have sent a password reset link to your email."});
	} catch (error) {
		res.status(400).json(error);
	}
});

module.exports = route;