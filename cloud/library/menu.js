const exportFunc = {};
const mealController = require('./meal');
const Parse = require('parse/node');
const Meal = Parse.Object.extend("Meal");
const Menu = Parse.Object.extend("Menu");
const Vendor = Parse.Object.extend("Vendor");

// constant
const DEFAULT_DATE_START = new Date('1970-01-03'); // 1970-01-04
const DEFAULT_DATE_END = new Date('1970-01-12');   // 1970-01-11
exportFunc.DEFAULT_DATE_START = DEFAULT_DATE_START;
exportFunc.DEFAULT_DATE_END = DEFAULT_DATE_END;

const toUnifiedDate = (datestr) => {
	const date = new Date(datestr + (datestr.indexOf("Z") === datestr.length -1 ? "" : " GMT+0"));
	date.setUTCHours(0, 0, 0, 0); // setHours set hours, minutes, seconds, milliseconds
	return date;
};
exportFunc.toUnifiedDate = toUnifiedDate;

const getDefaultDate = (day) => {
  const date = new Date(DEFAULT_DATE_START.getTime());
  date.setDate(date.getDate() + day + 1);
  return date;
};
exportFunc.getDefaultDate = getDefaultDate;

const getMinimumEditableDate = () => {
  const date = new Date();
  date.setDate(date.getDate() + 2);
  return toUnifiedDate(date.toDateString());
};
exportFunc.getMinimumEditableDate = getMinimumEditableDate;

const getMinimumCancelableDate = () => {
  const date = new Date();
  date.setDate(date.getDate() + 1);
  return toUnifiedDate(date.toDateString());
};
exportFunc.getMinimumCancelableDate = getMinimumCancelableDate;

exportFunc.getMenuById = async(menuId, includeOwner) => {
	const query = new Parse.Query(Menu);
	if (includeOwner) {
		query.include("vendor");
		query.include("vendor.rolePtr");
	}
  return await query.get(menuId);
};

//return {meals, prices} if valid, {type, message} if invalid
const verifyMealList = async(mealsList, vendor) => {
  const query = new Parse.Query(Meal);
  query.equalTo("vendor", vendor);
  const existMeals = await query.find();
	let errorId;
	let duplicate = false;
	let priceInvalid = 0;
	const meals = [];
	const prices = {};
	
	// find if there is any failed validation, such as duplicated meal, meal that does not exist
	const foundDiff = mealsList.some(function(mealData, index){
		//mealData.meal = id
		const mealId = mealData.id;
		
		// check if hashmap contains current mealID, if so, duplicate exists
		if(prices[mealId]){
			errorId = mealId;
			duplicate = true;
			return true;
		}
		
		// verify price
		prices[mealId] = { index: index, price: mealData.price };
		if(mealData.price < 0 || mealData.price > 350) {
			errorId = mealId;
			priceInvalid = mealData.price < 0 ? -1 : 1;
			return true;
		}
		
		// verify meal exists in database
		const containsMeal = existMeals.find(function(existMeal){
			return existMeal.id === mealId;
		});
		
		// invert it, as we are looking if it exist meals that are outside vendor meal list
		if (!containsMeal) {
			errorId = mealId;
		}
		
		// replace id string with parse object pointer (for referencing)
		meals.push(Meal.createWithoutData(mealId));
		
		return !containsMeal;
	});
	
	if (foundDiff) {
		const errorData = { id: errorId };
		if (priceInvalid) {
			errorData.type = "price";
			errorData.message = "The " + (priceInvalid === -1 ? "minimum" : "maximum") + " price for a meal is " + (priceInvalid === -1 ? "0" : "350") + " credit points.";
		} else if(duplicate) {
			errorData.type = "popup";
			errorData.message = "There's a duplicated meal added in the menu, please verify the menu and remove the duplicate!";
		} else {
			errorData.type = "popup";
			errorData.message = "It looks like one of the meal has been removed from the meal list.";
		}
		return errorData;
	}
	return {meals: meals, prices: prices};
};
exportFunc.verifyMealList = verifyMealList;

exportFunc.getValidatedData = async (req, vendor) => {
	const data = {};
	data.message = [];
	
	if (req.params.id) {
		data.id = req.params.id;
	}
	
	let invalidDate = -1;
	if (req.body._date) {
		data.date = Date.parse(req.body._date);
		if (isNaN(data.date)) {
			invalidDate = -1;
		} else {
			data.date = toUnifiedDate(req.body._date);
			invalidDate = (data.date >= getMinimumEditableDate() || (data.date >= DEFAULT_DATE_START && data.date <= DEFAULT_DATE_END)) ? 0 : 1;
		}
	}
	
	data.isLunch = req.body.isLunch || false;
	
	if (invalidDate) {
		data.message.push({
			type: "date",
			message: (invalidDate === -1 ? "Please select a valid date." : "Selected date are not creatable or modifiable.")
		});
	}
	
	const mealPriceArray = await verifyMealList(req.body.meals, vendor);
	if (mealPriceArray.type) {
		data.message.push(mealPriceArray);
	} else {
		data.meals = mealPriceArray.meals;
		data.prices = mealPriceArray.prices;
	}
	
	return data;
};

exportFunc.createMenuRecord = (vendor, menuData) => {
	const menu = new Menu();
	// Access control list
	const acl = new Parse.ACL();
	acl.setPublicReadAccess(true);
	acl.setRoleWriteAccess(vendor.get("rolePtr"), true);
	menu.setACL(acl);
	
	if (menuData.id) {
		menu.id = menuData.id;
	}
	menu.set("vendor", vendor);
	menu.set("date", menuData.date);
	menu.set("isLunch", menuData.isLunch);
	if(menuData.meals.length > 0){
		const mealRelation = menu.relation("meals");
		mealRelation.add(menuData.meals);
	}
	menu.set("prices", menuData.prices);
	return menu;
};


// for some reason, Parse save() will return a read-only object. have to get to modify
exportFunc.removeExtraRelation = async (menuId, sessionToken) => {
	const menu = await (new Parse.Query(Menu)).get(menuId, {sessionToken: sessionToken});
	const prices = menu.get("prices");
	const mealsRelation = menu.relation("meals");
	const mealList = await mealsRelation.query().find();
	mealList.forEach(function(singleMealData){
		if(prices[singleMealData.id] == null) {
			mealsRelation.remove(singleMealData);
			return;
		}
	});
	await menu.save(null, {sessionToken: sessionToken});
};

const getMenuByVendorAndDate = async (vendorId, dateStr, typeText, excludeDefaultQuery) => {
  if (typeText !== "breakfast" && typeText !== "lunch") {
    throw {code: 300, type: "type", message: "Please select either breakfast or lunch."};
  }
  const isLunch = typeText === "lunch";

  const currentDate = toUnifiedDate(dateStr);
  if (isNaN(currentDate.getTime())) {
    throw {code: 300, type: "date", message: "Please select a valid date."};
  }
  const currentDateOffset = new Date(currentDate);
  const defaultDate = getDefaultDate(currentDate.getUTCDay());
  const defaultDateOffset = new Date(defaultDate);

  currentDate.setUTCHours(-2);
  currentDateOffset.setUTCHours(2);
  defaultDate.setUTCHours(-2);
  defaultDateOffset.setUTCHours(2);

  const vendorQuery = new Parse.Query(Menu);
  vendorQuery.equalTo("vendor", Vendor.createWithoutData(vendorId));
  vendorQuery.equalTo("isLunch", isLunch);

  const selectedDateQuery = new Parse.Query(Menu);
  selectedDateQuery.greaterThanOrEqualTo("date", currentDate);
  selectedDateQuery.lessThan("date", currentDateOffset);

	let query;
	if (excludeDefaultQuery) {
		query = Parse.Query.and(vendorQuery, selectedDateQuery);
		query.include("vendor");
		query.include("vendor.rolePtr");
  	return await query.first();
	} else {
		const defaultDateQuery = new Parse.Query(Menu);
		defaultDateQuery.greaterThanOrEqualTo("date", defaultDate);
		defaultDateQuery.lessThan("date", defaultDateOffset);
		query = Parse.Query.and(vendorQuery, Parse.Query.or(selectedDateQuery, defaultDateQuery));
		//includes meals subdocument into results
		query.include("meals");
  	return await query.find();
	}
};
exportFunc.getMenuByVendorAndDate = getMenuByVendorAndDate;

const getMealListFromRelation = async (menu, useHashMap, withExtraDetails, onlyCalories) => {
	const meals = useHashMap ? {} : [];
	const prices = menu.get("prices");
  const mealsQuery = menu.relation("meals").query();
  mealsQuery.include("picture");

	const mealList = await mealsQuery.find();
	let converToMealData;
	if (useHashMap) {
		if (withExtraDetails) {
			if (onlyCalories) {
				converToMealData = function(singleMeal){
					meals[singleMeal.id] = singleMeal.get("calories");
				};
			} else {
				converToMealData = function(singleMeal){
					const singleMealObject = mealController.toMealData(singleMeal, false, prices[singleMeal.id].price);
					meals[singleMeal.id] = singleMealObject;
				};
			}
		} else {
			converToMealData = function(singleMeal){
				meals[singleMeal.id] = singleMeal.get("name");
			};
		}
	} else {
		converToMealData = function(singleMeal){
			const singleMealObject = mealController.toMealData(singleMeal, false, prices[singleMeal.id].price);
			meals[prices[singleMeal.id].index] = singleMealObject;
		};
	}
	mealList.forEach(converToMealData);
	return meals;
};
exportFunc.getMealListFromRelation = getMealListFromRelation;

exportFunc.toMenuData = async (menu, dateReceived, hideIdIfDefault, withExtraDetails) => {
  const menuData = {};
	const selectedDate = dateReceived instanceof Date ? dateReceived : toUnifiedDate(dateReceived);
	if(!hideIdIfDefault || menu.get("date") > DEFAULT_DATE_END) {
		menuData.id = menu.id;
	}
  menuData.date = selectedDate;
  menuData.isLunch = menu.get("isLunch");
	menuData.meals = await getMealListFromRelation(menu, withExtraDetails, withExtraDetails);
	
  return menuData;
};

exportFunc.generateMenuCopyFromDate = async(menu, date) => {
	// find if menu exist
	const dbMenu = await getMenuByVendorAndDate(menu.get("vendor").id, date.toUTCString(), menu.get("isLunch") ? "lunch" : "breakfast", true);

	if (dbMenu) {
		return dbMenu;
	}

	const copyMenu = new Menu();
	const acl = new Parse.ACL();
	acl.setPublicReadAccess(true);
	acl.setRoleWriteAccess(menu.get("vendor").get("rolePtr"), true);

	copyMenu.setACL(acl);
	copyMenu.set("date", date);
	copyMenu.set("vendor", menu.get("vendor"));
	copyMenu.set("prices", menu.get("prices"));
	copyMenu.set("isLunch", menu.get("isLunch"));
	const mealsRelation = copyMenu.relation("meals");
	const meals = await (menu.relation("meals").query().find());
	meals.forEach(meal => {
		mealsRelation.add(meal);
	});
	return await copyMenu.save(null, {useMasterKey: true});
};

module.exports = exportFunc;