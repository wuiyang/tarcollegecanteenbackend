const express = require('express');
const Parse = require('parse/node');
const feedbackController = require('../../library/feedback');
const route = express.Router();

route.post('/new', async (req, res) => {
  try {
    if ((res.locals.accessLevel === feedbackController.OPERATOR_LEVEL) ||
        (req.body.orderId && res.locals.accessLevel !== feedbackController.CUSTOMER_LEVEL)) {
      throw new Parse.Error(300, "You do not have access!");
    }
    const feedbackData = feedbackController.getValidatedData(req);
    if (feedbackData.message.length > 0) {
      throw new Parse.Error(300, feedbackData.message);
    }
    const feedback = await feedbackController.createFeedbackRecord(req.user, feedbackData, res.locals.sessionToken);
    await feedback.save(null, {sessionToken: res.locals.sessionToken});
    res.status(200).json({code: 1, message: "Successfully created feedback!", id: feedback.id});
  } catch (error) {
    res.status(400).json(error);
  }
});

route.get('/get/:id', async(req, res) => {
  try {
    if (req.params.id === "all") {
      const simleFeedbackList = await feedbackController.getSimpleFeedbackList(res.locals.sessionToken);
      res.status(200).json({code: 2, message: "Retrieved feedback list.", data: simleFeedbackList});
    } else if (feedbackController.isValidInput(req.params.id, /[0-9a-zA-Z]{10}/)) {
      const feedback = await feedbackController.getFeedbackById(req.params.id, res.locals.sessionToken, res.locals.accessLevel);
      res.status(200).json({code: 1, message: "Retrieved feedback.", data: feedback});
    } else {
      throw new Parse.Error(300, "Oops, something must have been messed up! Please refresh the page");
    }
  } catch (error) {
    res.status(400).json(error);
  }
});

route.post('/reply/:id', async(req, res) => {
  try {
    if (!feedbackController.isValidInput(req.params.id, /[0-9a-zA-Z]{10}/)) {
      throw new Parse.Error(300, "Oops, something must have been messed up! Please refresh the page");
    }
    const feedbackData = feedbackController.getValidatedData(req);
    const feedbackReply = await feedbackController.createFeedbackReplyRecord(feedbackData, res.locals.sessionToken, res.locals.accessLevel);
    await feedbackReply.save(null, {sessionToken: res.locals.sessionToken});
    res.status(200).json({code: 1, message: "Successfully sent feedback reply."});
  } catch (error) {
    res.status(400).json(error);
  }
});

module.exports = route;