const exportFunc = {};
const orderController = require('./order');
const Parse = require('parse/node');
const Feedback = Parse.Object.extend("Feedback");
const CUSTOMER_LEVEL = 0;
const OPERATOR_LEVEL = 2;

exportFunc.CUSTOMER_LEVEL = CUSTOMER_LEVEL;
exportFunc.OPERATOR_LEVEL = OPERATOR_LEVEL;

const isValidInput = (str, regex) => {
	if(str == null) {
		return false;
  }
	return regex.test(str.trim());
};
exportFunc.isValidInput = isValidInput;

const getOperatorRole = async () => {
  const query = new Parse.Query(Parse.Role);
  query.equalTo("name", "Canteen Operator");
  return await query.first();
};

exportFunc.getValidatedData = (req) => {
  const data = {};
  data.message = [];
  
	if (isValidInput(req.body.message, /.{10}/)){
		data.feedbackMessage = req.body.message.trim();
	} else {
		data.message.push({
			type: "message",
			message: "Please enter at least 10 characters for feedback message"
		});
  }

  // if is reply
  if (isValidInput(req.body.id, /[0-9a-zA-Z]{10}/)) {
    data.followFeedbackId = req.body.id;
  } else if (req.body.id && req.body.id.length > 0) {
		data.message.push({
			type: "popup",
			message: "Oops, something must have been messed up! Please refresh the page"
		});
  } else { // else is new
    // feedback to order (vendor)
    if (isValidInput(req.body.orderId, /[0-9a-zA-Z]{10}/)) {
      data.orderId = req.body.orderId;
    } else if (req.body.orderId && req.body.orderId.length > 0) {
      data.message.push({
        type: "popup",
        message: "Oops, something must have been messed up! Please refresh the page"
      });
    }
    // else is feedback to system (operator), left empty
  }
  return data;
};

const createFeedbackRecord = async (user, feedbackData, sessionToken) => {
  const feedback = new Feedback();
  const acl = new Parse.ACL();
  acl.setReadAccess(user, true);
  if (feedbackData.orderId) {
    const order = await orderController.getOrderById(feedbackData.orderId, sessionToken, true, true);
    const vendorAcl = order.get("menu").get("vendor").get("rolePtr");
    acl.setRoleReadAccess(vendorAcl, true);

    feedback.set("order", order);
  } else {
    const operatorRole = await getOperatorRole();
    acl.setReadAccess(operatorRole, true);
  }
  feedback.setACL(acl);

  feedback.set("message", feedbackData.feedbackMessage);
  feedback.set("isFeedbacker", true);
  return feedback;
};
exportFunc.createFeedbackRecord = createFeedbackRecord;

const createFeedbackReplyRecord = async (feedbackData, sessionToken, userLevel) => {
  const query = new Parse.Query(Feedback);
  const oldFeedback = await query.get(feedbackData.followFeedbackId, {sessionToken: sessionToken});
  const feedback = new Feedback();
  feedback.setACL(oldFeedback.getACL());

  let isUserFeedbacker;

  if (oldFeedback.has("order")) {
    isUserFeedbacker = userLevel === CUSTOMER_LEVEL;
  } else {
    isUserFeedbacker = userLevel !== OPERATOR_LEVEL;
  }

  feedback.set("message", feedbackData.feedbackMessage);
  feedback.set("isFeedbacker", isUserFeedbacker);
  feedback.set("followFeedback", oldFeedback);
  return feedback;
};
exportFunc.createFeedbackReplyRecord = createFeedbackReplyRecord;

const toFeedbackReplyData = (feedbackMain, feedbackReplyList, userLevel) => {
  const feedbackData = { id: feedbackMain.id, messages: [] };
  let isUserFeedbacker;
  if (feedbackMain.get("order")) {
    feedbackData.orderId = feedbackMain.get("order").id;
    isUserFeedbacker = userLevel === CUSTOMER_LEVEL;
  } else {
    isUserFeedbacker = userLevel !== OPERATOR_LEVEL;
  }
  feedbackData.messages.push({
    message: feedbackMain.get("message"),
    isSelfSend: feedbackMain.get("isFeedbacker") === isUserFeedbacker,
    date: feedbackMain.createdAt
  });
  feedbackReplyList.forEach(feedback => {
    feedbackData.messages.push({
      message: feedback.get("message"),
      isSelfSend: feedback.get("isFeedbacker") === isUserFeedbacker,
      date: feedback.createdAt
    });
  });
  return feedbackData;
};
exportFunc.toFeedbackReplyData = toFeedbackReplyData;

const toSingleFeedbackData = (feedback) => {
  const feedbackData = { id: feedback.id };
  if (feedback.get("order")) {
    feedbackData.orderId = feedback.get("order").id;
  }
  feedbackData.message = feedback.get("message");
  return feedbackData;
};
exportFunc.toSingleFeedbackData = toSingleFeedbackData;

exportFunc.getSimpleFeedbackList = async (sessionToken) => {
  const query = new Parse.Query(Feedback);
  query.doesNotExist("followFeedback");
  query.descending("createdAt");
  const simpleFeedbackList = await query.find({sessionToken: sessionToken});
  return simpleFeedbackList.map(feedback => toSingleFeedbackData(feedback));
};

exportFunc.getFeedbackById = async (feedbackId, sessionToken, userLevel) => {
  const query = new Parse.Query(Feedback);
  query.equalTo("followFeedback", Feedback.createWithoutData(feedbackId));
  const feedbackReplyList = await query.find({sessionToken: sessionToken});
  const feedbackMain = await (new Parse.Query(Feedback)).get(feedbackId, {sessionToken: sessionToken});

  return toFeedbackReplyData(feedbackMain, feedbackReplyList, userLevel);
};


module.exports = exportFunc;