const express = require('express');
const mealController = require('../../library/meal');
const route = express.Router();

const verifyMealData = (req, res, next) => {
	const mealData = mealController.getValidatedData(req);
	if(mealData.message.length !== 0){
		res.status(400).json({code: 300, message: mealData.message});
	}else{
		res.locals.mealData = mealData;
		next();
	}
};

const saveMealData = async(req, res) => {
  try {
		const mealData = res.locals.mealData;
		if (mealData.id) {
			const tempMeal = await mealController.getMealById(mealData.id);
			if (tempMeal.get("picture").id !== mealData.picture.id) {
				await tempMeal.get("picture").destroy({sessionToken: res.locals.sessionToken});
			}
		}
    const meal = mealController.createMealRecord(res.locals.vendor, mealData, req.params.id);
    await meal.save(null, {sessionToken: res.locals.sessionToken});

    res.status(req.params.id ? 200 : 201).json({code: 1, message: "Successfully created new meal", id: meal.id});
  } catch(error) {
    res.status(400).json(error);
  }
};

route.post('/new', verifyMealData, saveMealData);

route.post('/modify/:id', verifyMealData, saveMealData);

route.get('/all',
	async (req, res) => {
		try {
			const mealList = await mealController.getMealsByVendor(res.locals.vendor, true);
			res.status(200).json({code: 2, message: "Retrieved meal list for vendor.", data: mealList});
		} catch (error) {
			res.status(400).json(error);
		}
	}
);

route.delete('/delete/:id',
	async (req, res) => {
    try {
			await mealController.removeMealRecord(req.params.id, res.locals.sessionToken);
			res.status(200).json({code: 1, message: "Successfully deleted meal", id: req.params.id});
    } catch(error) {
			res.status(400).json(error);
    }
	}
);

module.exports = route;