const Parse = require('parse/node');
const menuController = require('./library/menu');
const orderController = require('./library/order');
const creditController = require('./library/credit');
const Picture = Parse.Object.extend("Picture");
const Menu = Parse.Object.extend("Menu");
const Order = Parse.Object.extend("Order");
const Credit = Parse.Object.extend("Credit");

Parse.Cloud.define('ping', async () => {
  return "Pong";
});

Parse.Cloud.beforeSave("CreditTransaction", async(request) => {
  const tempAcl = request.object.getACL();
  const hasWriteAccess = tempAcl.getWriteAccess(request.object.get("fromUser")) || tempAcl.getWriteAccess(request.object.get("toUser"));
  if(request.object.get("verified") && (hasWriteAccess || request.object.id == null)){
    const isFromQuery = new Parse.Query(Credit);
    const fromCredit = await isFromQuery.equalTo("user", request.object.get("fromUser")).first({useMasterKey: true});
    const isToQuery = new Parse.Query(Credit);
    const toCredit = await isToQuery.equalTo("user", request.object.get("toUser")).first({useMasterKey: true});
    
    // do not allow if credit is insufficient
    // if it is refund of preorder, then allow vendor to have negative credit
    if (fromCredit.get("credit") < request.object.get("amount") &&
        (request.object.get("preorder") == null || request.user.id !== request.object.get("fromUser").id)) {
      throw new Parse.Error(142, "Insufficient balance to complete transaction!");
    }

    fromCredit.set("credit", fromCredit.get("credit") - request.object.get("amount"));
    toCredit.set("credit", toCredit.get("credit") + request.object.get("amount"));

    request.object.set("fromCredit", fromCredit.get("credit"));
    request.object.set("toCredit", toCredit.get("credit"));

    request.object.set("date", new Date());

    // remove any writable ACL
    if(request.object.id){
      const acl = new Parse.ACL();
      acl.setPublicReadAccess(false);
      acl.setPublicWriteAccess(false);
      acl.setReadAccess(request.object.get("fromUser"), true);
      acl.setReadAccess(request.object.get("toUser"), true);
      request.object.setACL(acl);
    }

    await fromCredit.save(null, {useMasterKey: true});
    await toCredit.save(null, {useMasterKey: true});

    return request.object;
  }
});

Parse.Cloud.beforeSave("Credit", async(request) => {
  const user = await request.object.get("user").fetch();
  if (user.get("accessLevel") === 2) {
    request.object.set("credit", 999999999);
  }
  return request.object;
});

Parse.Cloud.beforeDelete('Meal', async(request) => {
  const query = new Parse.Query(Menu);
  query.equalTo("meals", request.object);
  const menu = await query.first();
  if (menu) {
    throw new Parse.Error(300, "Unable to delete the following meal, one of the menu are contains this meal.");
  }
  const picture = Picture.createWithoutData(request.object.get("picture").id);
  await picture.destroy({ sessionToken: request.user.getSessionToken() });
  return request.object;
});

Parse.Cloud.beforeSave('Menu', async(request) => {
  if (!request.object.id) {
    return;
  }
  const oldprices = request.original.get("prices");
  const prices = request.object.get("prices");
  const difference = [];

  // only check deleted old menu meal, modified are allowed (as promotion or early bird)
  Object.keys(oldprices).forEach((mealId) => {
    if (prices[mealId] == null) {
      difference.push(mealId);
    }
  });

  await Promise.all(difference.map(async(mealId) => {
    const query = new Parse.Query(Order);
    query.equalTo("menu", request.object);
    const results = await query.find({sessionToken: request.user.getSessionToken()});
    results.forEach((order) => {
      if (order.get("details").find(detail => detail.mealId === mealId)) {
        throw new Parse.Error(300, "Unable to modify the following menu, preorder has been made on this menu with the selected meal.");
      }
    });
  }));

  return request.object;
});

Parse.Cloud.beforeDelete('Menu', async(request) => {
  const query = new Parse.Query(Order);
  query.equalTo("menu", request.object);
  const order = await query.first({ sessionToken: request.user.getSessionToken() });
  if (order) {
    throw new Parse.Error(300, "Unable to delete the following menu, preorder has been made on this menu.");
  }
  return request.object;
});

const calculateTotal = (menu, order) => {
  if (order == null) {
    return;
  }
  const prices = menu.get("prices");
  let total = 0;
  order.get("details").forEach(orderDetail => { total += prices[orderDetail.mealId].price * orderDetail.quantity; });
  return total;
};

// for create, there will be date attribute, which stores menu order date (not actual menu's date attribute)
Parse.Cloud.beforeSave('Order', async(request) => {
  if (request.original && request.original.cancel) {
    throw new Parse.Error(119, 'You are not allowed to modify cancelled preorder.');
  }

  if (request.master) {
    if (request.object.has("completeTime") && !request.original.has("completeTime")) {
      return;
    }
    if (request.object.has("receiveTime") && !request.original.has("receiveTime")) {
      return;
    }
  }

  let menu = await request.object.get("menu").fetch({sessionToken: request.user.getSessionToken(), include: ["vendor", "vendor.rolePtr"]});

  // verify if order is creatable, modifiable, or cancellable
  const menuDate = request.object.get("menuDate") || menu.get("date");
  const type = request.object.get("menuDate") ? "create" : request.object.get("cancel") ? "cancel" : "modify";
  if (type !== "cancel") {
    orderController.verifyOrder(menu, request.object.get("details"), menuDate, type);
  } else {
    const acl = request.object.getACL();
    acl.setWriteAccess(request.user, false);
    request.object.setACL(acl);
    // do not allow user to modify after cancel
  }

  const beforeTotal = calculateTotal(menu, request.original) || 0;
  const currentTotal = request.object.get("cancel") ? 0 : calculateTotal(menu, request.object);
  const payAmount = currentTotal - beforeTotal; // +payAmount = user -> vendor, -payAmount = user <- vendor
  let description;

  // stop processing if no modification is made
  if (payAmount === 0) {
    throw new Parse.Error(142, 'No modification is made.');
  }
  
  if (menu.get("date") <= menuController.DEFAULT_DATE_END) { // only possible for create
    menu = await menuController.generateMenuCopyFromDate(menu, request.object.get("menuDate"));
    request.object.set("menu", menu);
  }

  if (beforeTotal > 0 && currentTotal > 0) { // modify
    description = "Modified preorder for " + menu.get("date").toDateString() + " " + (menu.get("isLunch") ? "lunch" : "breakfast");
    description += orderController.getModifiedDetail(request.original.get("details"), request.object.get("details"), await menuController.getMealListFromRelation(menu, true));
  } else if (currentTotal > 0) { // new
    description = "Payment for " + menu.get("date").toDateString() + " " + (menu.get("isLunch") ? "lunch" : "breakfast") + " preorder";
    request.object.unset("menuDate"); // remove date
  } else { // cancel
    description = "Cancelled preorder for " + menu.get("date").toDateString() + " " + (menu.get("isLunch") ? "lunch" : "breakfast");
  }

  if (payAmount > 0) {
    // check enough balance
    const credit = await (new Parse.Query(Credit)).first({sessionToken: request.user.getSessionToken()});
    if (credit.get("credit") < payAmount) {
      throw new Parse.Error(142, "Not enough credit points to purchase meal!");
    }
  }

  request.context = {oppositeUser: menu.get("vendor").get("rolePtr").get("owner"), description: description, amount: payAmount};
});

Parse.Cloud.afterSave('Order', async(request) => {
  const amount = request.context.amount;
  const creditTransaction = creditController.createTransactionRecord({
    fromUser: amount > 0 ? request.user.id : request.context.oppositeUser.id,
    toUser: amount > 0 ? request.context.oppositeUser.id : request.user.id,
    amount: Math.abs(amount),
    description: request.context.description,
    preorder: request.object,
    verified: true
  }, request.user, request.context.oppositeUser);
  await creditTransaction.save(null, {sessionToken: request.user.getSessionToken()});
});