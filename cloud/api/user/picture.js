const express = require('express');
const pictureController = require('../../library/picture');
const vendorController = require('../../library/vendor');
const route = express.Router();

const getVendorId = async (req, res, next) => {
  try{
    const vendorInfo = await vendorController.getVendorByUser(req.user);
    if (vendorInfo) {
      res.locals.vendor = vendorInfo;
    }
    next();
  } catch(error) {
    res.status(400).json(error);
  }
};

route.post('/', getVendorId, async (req, res) => {
  try {
    const picture = pictureController.createPictureRecord(req.user, req.body.name, req.body.base64, res.locals.vendor.get("rolePtr"));
    await picture.save(null, {sessionToken: res.locals.sessionToken});
    
    res.status(201).json({
      code: 1,
      message: "Succcessfully created pictures.",
      id: picture.id,
      url: picture.get("picture").url()
    });
  } catch(error) {
    res.status(400).json(error);
  }
});

module.exports = route;