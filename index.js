//require modules and configurations
const express = require('express');
const ParseServer = require('parse-server').ParseServer;
const ParseDashboard = require('parse-dashboard');
const ParseConfig = require("./config.json");
const customApi = require("./cloud/api/api");
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

//initialize modules
const api = new ParseServer(ParseConfig); //new parse server api
const dashboard = new ParseDashboard({
  apps: [ParseConfig]
}); //new parse dashboard
const app = express(); //new express router

app.use(cookieParser("3Ep6yZlx2ZN0S5mmRS96pTizki105UFRgLTTeNW6A2JzG"));
app.use(bodyParser.json({ limit: '50mb' })); // for parsing application/json
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true })); // for parsing application/x-www-form-urlencoded


app.get('/', function(req, res) {
  res.sendFile(__dirname + '/public/menu.html');
});

//load public files
app.use('/public', express.static(__dirname + '/public'));

//Serve the Parse API on the defined URL prefix
const mountPath = "/" + ParseConfig.path;
app.use(mountPath, api);

//Dashboard
app.use('/dashboard', dashboard);

app.use('/api', customApi);

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function(req, res) {
  res.sendFile(__dirname + '/public/test.html');
});

app.get('/ping', function(req, res) {
  res.status(200).send('pong');
});

const port = ParseConfig.port;
const httpServer = require('http').createServer(app);
httpServer.listen(port, 'localhost', function() {
    console.log('Canteen backend server running on port ' + port + '.');
});