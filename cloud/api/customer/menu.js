const express = require('express');
const menuController = require('../../library/menu');
const vendorController = require('../../library/vendor');
const route = express.Router();

route.get('/get', async(req, res) => {
  try {
    const vendorDataList = await vendorController.getVendorList();
    res.status(200).json({ code: 2, message: "Retrieved vendor list.", data: vendorDataList });
  } catch (error) {
    res.status(400).json(error);
  }
});

route.get('/get/:id/:date/:type',
  async (req, res) => {
    try{
      const menus = await menuController.getMenuByVendorAndDate(req.params.id, req.params.date, req.params.type);
      if (menus == null || menus.length === 0) {
        res.status(200).json({code: 3, message: "No menu exists for this vendor."});
        return;
      }

      let menu = menus[0];
      let isDefault = menu.get("date") <= menuController.DEFAULT_DATE_END;

      if (menus.length === 2 && isDefault) {
        menu = menus[1];
        isDefault = false;
      }

      const menuData = await menuController.toMenuData(menu, req.params.date);
      res.status(200).json({code: 2, message: "Retrieved menu list from this vendor.", data: menuData});
    } catch(error) {
      res.status(400).json(error);
    }
  }
);

module.exports = route;