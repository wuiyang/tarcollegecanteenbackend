const exportFunc = {};
const Parse = require('parse/node');
const Meal = Parse.Object.extend("Meal");
const Picture = Parse.Object.extend("Picture");

const isValidInput = (str, regex) => {
	if(str == null) {
		return false;
	}
	
	return regex.test(str.trim());
};

const getValidatedData = (req) => {
	const data = {};
	data.message = [];
	
	if (req.params.id) {
		data.id = req.params.id;
	}
	
	if (isValidInput(req.body.name, /.{3,}/)) {
		data.name = req.body.name.trim();
	} else {
		data.message.push({
			type: "name",
			message: "Meal name must have at least 3 characters!"
		});
	}
	
	data.description = (req.body.description || '').trim();
	
	if (typeof req.body.price === "number") {
		if (req.body.price < 0 || req.body.price > 350) {
			data.message.push({
				type: "refPrice",
				message: "The " + (req.body.price < 0 ? "minimum" : "maximum") + " price for a meal is " + (req.body.price < 0 ? "0" : "350") + " credit points."
			});
		} else {
			data.refPrice = req.body.price;
		}
	} else {
		data.message.push({
			type: "refPrice",
			message: "Meal reference price must be in positive number form without decimal."
		});
	}
	
	if (typeof req.body.calories === "number") {
		if (req.body.calories < 0 || req.body.calories > 1800) {
			data.message.push({
				type: "calories",
				message: req.body.calories < 0 ? "Please enter a valid calories amount." : "The calories in this meal exceeds 90% of recommended daily calories intake!"
			});
		} else {
			data.calories = req.body.calories;
		}
	} else {
		data.message.push({
			type: "calories",
			message: "Meal calories must be in positive number form without decimal."
		});
	}

	if (isValidInput(req.body.pictureId, /^[0-9a-zA-Z]{10}$/)) {
		data.picture = Picture.createWithoutData(req.body.pictureId);
	} else {
		data.message.push({
			type: "popup",
			message: "Please upload a display picture for meal."
		});
	}
	
	data.ingredients = [];
	req.body.ingredients.some(function(ingredient){
		if (ingredient.name == null || ingredient.name.length < 1) {
			data.message.push({
				type: "popup",
				message: "Please enter ingredient name."
			});
		}
		if (ingredient.quantity == null || ingredient.quantity < 0.001) {
			data.message.push({
				type: "popup",
				message: "Minimum ingredient quantity is 1."
			});
		}
		data.ingredients.push({name: ingredient.name, quantity: ingredient.quantity, unit: ingredient.unit});
		return data.message.length > 0;
	});
	
	return data;
};
exportFunc.getValidatedData = getValidatedData;

const createMealRecord = (vendor, mealData, id) => {
	const meal = new Meal();
	// Access control list
	const acl = new Parse.ACL();
	acl.setPublicReadAccess(true);
	acl.setRoleWriteAccess(vendor.get("rolePtr"), true);
	meal.setACL(acl);
	
	if(id) {
		meal.id = id;
	}
	meal.set("vendor", vendor);
	meal.set("name", mealData.name);
	meal.set("description", mealData.description);
	meal.set("refPrice", mealData.refPrice);
	meal.set("calories", mealData.calories);
	meal.set("picture", mealData.picture);
	meal.set("ingredients", mealData.ingredients);
	return meal;
};
exportFunc.createMealRecord = createMealRecord;

const toMealData = (meal, includePictureId, customPrice) => {
  const mealData = {};
  mealData.id = meal.id;
  mealData.name = meal.get("name");
  mealData.description = meal.get("description");
  mealData.price = customPrice || meal.get("refPrice");
  mealData.calories = meal.get("calories");
  if (includePictureId) {
    mealData.pictureId = meal.get("picture").id;
  }
  mealData.pictureUrl = (meal.get("picture") && meal.get("picture").get("picture").url()) || '';
  mealData.ingredients = meal.get("ingredients");
  return mealData;
};
exportFunc.toMealData = toMealData;

const toMealsDataList = (meals, includePictureId) => {
  const mealsData = [];
  meals.forEach(meal => { mealsData.push(toMealData(meal, includePictureId)); });
  return mealsData;
};
exportFunc.toMealsDataList = toMealsDataList;

exportFunc.getMealsByVendor = async (vendor, includePictureId) => {
	const query = new Parse.Query(Meal);
  query.equalTo("vendor", vendor);
  if (includePictureId) {
    query.include("picture");
  }
	
  const meals = await query.find();
  return toMealsDataList(meals, includePictureId);
};


const getMealById = async (mealId) => {
	return await (new Parse.Query(Meal)).get(mealId);
};
exportFunc.getMealById = getMealById;

exportFunc.removeMealRecord = async (mealId, sessionToken) => {
	const meal = await getMealById(mealId);
	await meal.get("picture").destroy({sessionToken: sessionToken});
  return await meal.destroy({sessionToken: sessionToken});
};

module.exports = exportFunc;