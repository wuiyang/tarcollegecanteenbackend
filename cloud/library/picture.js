const exportFunc = {};
const Parse = require('parse/node');
const Picture = Parse.Object.extend("Picture");

exportFunc.createPictureRecord = (user, name, base64, vendorRole) => {
  const picture = new Picture();
  const acl = new Parse.ACL();
  acl.setPublicReadAccess(true);
  if (vendorRole) {
    acl.setRoleWriteAccess(vendorRole, true);
  } else if (user) {
    acl.setWriteAccess(user, true);
  }
  picture.setACL(acl);
  picture.set("picture", new Parse.File(name, { base64: base64 }));
  return picture;
};

exportFunc.removePictureById = (pictureId, sessionToken) => {
  Picture.createWithoutData(pictureId).destroy({sessionToken: sessionToken});
};

module.exports = exportFunc;