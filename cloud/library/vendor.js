const exportFunc = {};
const Parse = require('parse/node');
const Vendor = Parse.Object.extend("Vendor");
const Picture = Parse.Object.extend("Picture");

const isValidInput = (str, regex) => {
	if(str == null) {
		return false;
	}
	
	return regex.test(str.trim());
};

exportFunc.getValidatedData = (req) => {
  const data = {};
  data.message = [];

  if (req.body.id.length === 0 || req.body.id.length === 10) {
		data.id = req.body.id;
	} else {
		data.message.push({
			type: "popup",
			message: "Something went wrong, please refresh the page."
		});
  }

  if (isValidInput(req.body.name, /.{4,}/)) {
    data.name = req.body.name.trim();
  } else {
    data.message.push({
			type: "name",
			message: "Store name must be at least 4 characters long."
		});
  }

  data.description = (req.body.description || '').trim();

  if (req.body.pictureId.length === 10) {
		data.picture = Picture.createWithoutData(req.body.pictureId);
	} else {
		data.message.push({
			type: "popup",
			message: "Please upload a display picture for your store."
		});
  }
  return data;
};

exportFunc.updateVendorRecord = async (vendorInfo, vendorData, sessionToken) => {
  vendorInfo.set("name", vendorData.name);
  vendorInfo.set("description", vendorData.description);
  vendorInfo.set("picture", vendorData.picture);
  return await vendorInfo.save(null, {sessionToken: sessionToken});
};

const toVendorData = (vendorInfo, includePictureId) => {
  const vendorData = { id: vendorInfo.id };
  vendorData.name = vendorInfo.get("name");
  vendorData.description = vendorInfo.get("description");
  if (vendorInfo.get("picture")) {
    if (includePictureId) {
      vendorData.pictureId = vendorInfo.get("picture").id;
    }
    vendorData.pictureUrl = vendorInfo.get("picture").get("picture").url();
  }
  return vendorData;
};
exportFunc.toVendorData = toVendorData;

exportFunc.getVendorList = async () => {
  const query = new Parse.Query(Vendor);
  query.exists("picture");
  query.include("picture");

  const vendorList = await query.find();
  const vendorDataList = [];
  vendorList.forEach(function(vendor){
    vendorDataList.push(toVendorData(vendor, false));
  });
  return vendorDataList;
};

exportFunc.getVendorByUser = async (user, includePicture) => {
  const roleQuery = new Parse.Query(Parse.Role);
  roleQuery.equalTo("users", user);

  const query = new Parse.Query(Vendor);
  query.matchesQuery("rolePtr", roleQuery);
  if (includePicture) {
    query.include("picture");
  } else {
    query.include("rolePtr");
  }

  return await query.first();
};

exportFunc.getVendorById = async (vendorId) => {
  const query = new Parse.Query(Vendor);
  return await query.get(vendorId);
};

module.exports = exportFunc;
