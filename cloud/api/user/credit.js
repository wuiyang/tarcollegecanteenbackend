const express = require('express');
const Parse = require('parse/node');
const creditController = require('../../library/credit');
const userController = require('../../library/user');
const route = express.Router();
const Credit = Parse.Object.extend("Credit");
const OPERATOR = 2;

const verifyTransactionData = async (req, res, next) => {
  try {
    const transactionData = creditController.getValidatedData(req);
    if (transactionData.message.length > 0) {
      throw new Parse.Error(300, transactionData.message);
    }
    const oppositeUser = transactionData.fromUser === req.user.id ? transactionData.toUser : transactionData.fromUser;
    const user = await userController.getUserById(oppositeUser);
    if(user == null){
      throw new Parse.Error(101);
    }
    res.locals.transactionData = transactionData;
    res.locals.oppositeUser = user;
    next();
  } catch(error) {
    if (error.code === 101) {
      res.status(400).json({code: 300, message: [{
        type: "userId",
        message: "User ID does not exist!"
      }]});
    } else {
      res.status(400).json(error);
    }
  }
};

const getTransactionQuery = async (req, res) => {
  try {
    const transactionDataList = await creditController.getTransactionQuery(req.user.id, res.locals.sessionToken, req.params.month);
    res.status(200).json({code: 2, message: "Retrieved credit transactions list.", data: transactionDataList});
  } catch(error) {
    res.status(400).json(error);
  }
};

route.use(function(req, res, next){
  const query = new Parse.Query(Credit);
  // no need to query for user = current user, as have setup ACL control
  query.first({sessionToken: res.locals.sessionToken}).then(function(credit){
    if(credit == null){
      res.status(400).json({code: 101, message: "Credit info not found"});
    }else{
      res.locals.credit = credit;
      next();
    }
  }).catch(function(error){
    res.status(400).json(error);
  });
});

route.get('/info', function(req, res){
  const creditData = { amount: res.locals.credit.get("credit"), user: req.user.id };
  res.status(200).json({code: 1, message: "Retrieved credit info.", data: creditData});
});

route.get('/transactions', getTransactionQuery);

route.get('/topuplist',
  (req, res, next) => {
    if (res.locals.accessLevel === OPERATOR) {
      next();
    } else {
      res.status(400).json({code: 301, message: "You do not have access!"});
    }
  }, async (req, res) => {
    try {
      const transactionDataList = await creditController.getTransactionQuery(req.user.id, res.locals.sessionToken, null, true);
      res.status(200).json({code: 2, message: "Retrieved credit transactions list.", data: transactionDataList});
    } catch(error) {
      res.status(400).json(error);
    }
  }
);

// get transaction list by month, format: yyyymm
route.get('/transactions/:month', getTransactionQuery);

route.post('/transaction/create', verifyTransactionData,
  async (req, res) => {
    try {
      const transaction = creditController.createTransactionRecord(res.locals.transactionData, req.user, res.locals.oppositeUser);
      await transaction.save(null, {sessionToken: res.locals.sessionToken});
      res.status(201).json({code: 1, message: "Successfully created new transaction", id: transaction.id});
    } catch(error) {
      res.status(400).json(error);
    }
  }
);

route.post('/transaction/confirm/:id', 
  async (req, res) => {
    try {
      const transaction = await creditController.getTransactionById(req.params.id, res.locals.sessionToken);
      transaction.set("verified", true);
      await transaction.save(null, {sessionToken: res.locals.sessionToken});
      res.status(200).json({code: 1, message: "Successfully confirmed request transaction."});
    } catch(error) {
      res.status(400).json(error);
    }
  }
);

module.exports = route;