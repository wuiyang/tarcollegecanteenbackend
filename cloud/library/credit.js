const exportFunc = {};
const Parse = require('parse/node');
const CreditTransaction = Parse.Object.extend("CreditTransaction");

const isValidInput = (str, regex) => {
	if(str == null) {
		return false;
  }
	return regex.test(str.trim());
};

const getValidatedData = (req) => {
  const data = {};
  data.message = [];
  
  if(isValidInput(req.body.fromUser, /^[0-9a-zA-Z]{10}$/)){
    data.fromUser = req.body.fromUser;
  }else{
    data.message.push({
      type: "userId",
      message: "Invalid user ID entered!"
    });
  }

  if(isValidInput(req.body.toUser, /^[0-9a-zA-Z]{10}$/)){
    data.toUser = req.body.toUser;
  }else{
    data.message.push({
      type: "userId",
      message: "Invalid user ID entered!"
    });
  }

  // if either is not equal to current user ID, or both are equal to current user ID
  if((data.fromUser !== req.user.id && data.toUser !== req.user.id) || data.fromUser === data.toUser){
    data.message.push({
      type: "userId",
      message: data.fromUser === data.toUser ? "You can't transfer money to yourself!" : "You can't create transaction on behalf of other to other user!"
    });
  }

  data.verified = data.fromUser === req.user.id;

  if(req.body.description){
    data.description = req.body.description;
  }

  data.amount = req.body.amount;
  if(data.isTopup) {
    if(req.body.amount < 10 || req.body.amount > 2500){
      data.message.push({
        type: "amount",
        message: (req.body.amount < 1 ? "Minimum" : "Maximum") + " exchange amount is " + (req.body.amount < 1 ? "10" : "2500") + " credit point!"
      });
    }
  } else {
    if(req.body.amount < 1 || req.body.amount > 2500){
      data.message.push({
        type: "amount",
        message: (req.body.amount < 1 ? "Minimum" : "Maximum") + " transfer amount per transaction is " + (req.body.amount < 1 ? "1" : "2500") + " credit point!"
      });
    }
  }

  data.isTopup = req.body.isTopup || false;

  return data;
};
exportFunc.getValidatedData = getValidatedData;

const getTransactionDataFromObject = (transaction, userId) => {
  const transactionData = { id: transaction.id };
  transactionData.fromUser = transaction.get("fromUser").id;
  transactionData.toUser = transaction.get("toUser").id;
  transactionData.amount = transaction.get("amount");
  transactionData.date = transaction.get("date");
  transactionData.verified = transaction.get("verified");
  transactionData.description = transaction.get("description");
  transactionData.preorderId = (transaction.get("preorder") && transaction.get("preorder").id) || "";
  transactionData.balance = transaction.get(transaction.get("fromUser").id === userId ? "fromCredit" : "toCredit");
  return transactionData;
};
exportFunc.getTransactionDataFromObject = getTransactionDataFromObject;

exportFunc.createTransactionRecord = (transactionData, user, oppositeUser) => {
  const transaction = new CreditTransaction();
  const acl = new Parse.ACL();
  acl.setPublicReadAccess(false);
  acl.setPublicWriteAccess(false);
  acl.setReadAccess(user, true);
  acl.setReadAccess(oppositeUser, true);

  // not verified yet, allow modification
  if(!transactionData.verified){
    acl.setWriteAccess(user, true);
    acl.setWriteAccess(oppositeUser, true);
  }
  transaction.setACL(acl);
  transaction.set("fromUser", transactionData.fromUser === user.id ? user : oppositeUser);
  transaction.set("toUser", transactionData.toUser === user.id ? user : oppositeUser);
  transaction.set("amount", transactionData.amount);
  transaction.set("date", new Date());
  if(transactionData.description){
    transaction.set("description", transactionData.description);
  }
  if(transactionData.preorder){
    transaction.set("preorder", transactionData.preorder);
  }
  transaction.set("verified", transactionData.verified);
  transaction.set("fromCredit", 0);
  transaction.set("toCredit", 0);
  if (transactionData.isTopup) {
    transaction.set("isTopup", true);
  }
  return transaction;
};

exportFunc.getTransactionById = async (transactionId, sessionToken) => {
  return await (new Parse.Query(CreditTransaction)).get(transactionId, {sessionToken: sessionToken});
};

exportFunc.getTransactionQuery = async (userId, sessionToken, monthyearstr, isTopup) => {
  const query = new Parse.Query(CreditTransaction);
  if (monthyearstr) {
    const startMonth = new Date(monthyearstr.substr(0,4), monthyearstr.substr(4));
    const endMonth = new Date(monthyearstr.substr(0,4), +monthyearstr.substr(4) + 1, 0);
    query.greaterThanOrEqualTo("date", startMonth);
    query.lessThanOrEqualTo("date", endMonth);
  } else {
    query.limit(200);
  }
  if (isTopup) {
    query.equalTo("isTopup", true);
  }
  query.descending("date");
  const transactions = await query.find({sessionToken: sessionToken});
  const transactionDataList = [];
  transactions.forEach(function(transaction){
    transactionDataList.push(getTransactionDataFromObject(transaction, userId));
  });
  return transactionDataList;
};

module.exports = exportFunc;
