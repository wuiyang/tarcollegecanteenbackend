const express = require('express');
const Parse = require('parse/node');
const route = express.Router();
const accountApi = require('./account');
const creditApi = require('./user/credit');
const feedbackApi = require('./user/feedback');
const menuApi = require('./customer/menu');
const orderApi = require('./customer/order');
const pictureApi = require('./user/picture');
const vendorApi = require('./vendor/vendor');

const CUSTOMER_LEVEL = 0;
const VENDOR_LEVEL = 1;
const OPERATOR_LEVEL = 2;
const serverFrontendIp = "http://localhost:4200";

// Allow CORS
route.options('/*', function(req, res){
  res.setHeader('Access-Control-Allow-Origin', serverFrontendIp);
  res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'content-type');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.status(req.headers.origin === serverFrontendIp ? 200 : 400).send();
});

route.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', serverFrontendIp);
  res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'content-type');
  res.setHeader('Access-Control-Allow-Credentials', 'true');

  const cookie = req.signedCookies['parse.session'];
  res.locals.userLoggedIn = 0;
  if(cookie != null){
  res.locals.sessionToken = JSON.parse(cookie).sessionToken;
  Parse._request('GET', 'users/me', {}, {sessionToken: res.locals.sessionToken}).then(function (userData) {
    res.locals.userLoggedIn = 1;
    req.user = Parse.Object.fromJSON(userData);
    res.locals.accessLevel = req.user.get("accessLevel");
    next();
  }).catch(function () {
    res.locals.userLoggedIn = -1;
    res.clearCookie('parse.session');
    res.clearCookie('displayName');
    res.clearCookie('accessLevel');
    next();
  });
  }else{
    next();
  }
});

function verifyUserHasAccess(customerHasAccess, vendorHasAccess, operatorHasAccess) {
  return (req, res, next) => {
    try {
      if (res.locals.userLoggedIn === -1) {
        throw new Parse.Error(209,  "You session has expired.");
      } else if (res.locals.userLoggedIn === 0) {
        throw new Parse.Error(301,  "You have not login.");
      } else if ((res.locals.accessLevel === CUSTOMER_LEVEL && !customerHasAccess) ||
                 (res.locals.accessLevel === VENDOR_LEVEL && !vendorHasAccess) ||
                 (res.locals.accessLevel === OPERATOR_LEVEL && !operatorHasAccess)) {
        throw new Parse.Error(301, "You do not have access!");
      }
      next();
    } catch (error) {
      res.status(400).json(error);
    }
  };
}

route.use('/account', accountApi);
route.use('/credit', verifyUserHasAccess(true, true, true), creditApi);
route.use('/feedback', verifyUserHasAccess(true, true, true), feedbackApi);
route.use('/menu', menuApi);
route.use('/order', verifyUserHasAccess(true, false, true), orderApi);
route.use('/picture', verifyUserHasAccess(true, true, true), pictureApi);
route.use('/vendor', verifyUserHasAccess(false, true, false), vendorApi);

module.exports = route;